FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
  && apt-get install --yes --quiet --no-install-recommends \
    munge \
    runit \
    slurmctld \
    slurmdbd \
    tzdata \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN true \
  # munge
  && mkdir -p /var/run/munge && chown munge:munge /var/run/munge \
  && mkdir -p /etc/service/munge \
  && {\
    echo "#!/bin/bash"; \
    echo "set -e"; \
    echo "chown munge:munge /var/{lib,log,run}/munge"; \
    echo "chown -R munge:munge /etc/munge"; \
    echo "chmod 700 /etc/munge"; \
    echo "chmod 400 /etc/munge/munge.key"; \
    echo "exec chpst -u munge /usr/sbin/munged -F"; \
  } > /etc/service/munge/run \
  && touch /etc/service/munge/down \
  && chmod +x /etc/service/munge/run \
  # slurmdbd
  && mkdir -p /var/run/slurm && chown slurm /var/run/slurm \
  && mkdir -p /etc/service/slurmdbd \
  && {\
    echo "#!/bin/bash"; \
    echo "set -e"; \
    echo "chown slurm /etc/slurm/slurmdbd.conf"; \
    echo "chmod 600 /etc/slurm/slurmdbd.conf"; \
    echo "exec /usr/sbin/slurmdbd -D"; \
  } > /etc/service/slurmdbd/run \
  && chmod +x /etc/service/slurmdbd/run \
  && touch /etc/service/slurmdbd/down \
  # slurmctld
  && mkdir -p /var/run/slurm && chown slurm /var/run/slurm \
  && mkdir -p /var/spool/slurmctld && chown slurm /var/spool/slurmctld \
  && mkdir -p /etc/service/slurmctld \
  && {\
    echo "#!/bin/bash"; \
    echo "set -e"; \
    echo "exec /usr/sbin/slurmctld -D"; \
  } > /etc/service/slurmctld/run \
  && chmod +x /etc/service/slurmctld/run \
  && touch /etc/service/slurmctld/down

# slurmctld (6817), slurmd (6818), slurmdbd (6819)
EXPOSE 6817/tcp 6818/tcp 6819/tcp

CMD ["runsvdir", "/etc/service"]
