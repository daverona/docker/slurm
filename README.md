# daverona/slurm

```bash
docker image pull daverona/slurm
```

```bash
docker image build \
  --build-arg APP_TIMEZONE=UTC \
  --tag daverona/slurm
  .
```

## References

* https://github.com/mknoxnv/ubuntu-slurm
* https://www.slideshare.net/VinayJindal7/using-cgroups-in-docker-container
* https://github.com/giovtorres/slurm-docker-cluster
* https://github.com/mknoxnv/ubuntu-slurm
* https://github.com/SciDAS/slurm-in-docker
